provider "aws" {
  profile = "default"
  region  = "us-east-2"
}

output "alb_hostname" {
  value = aws_alb.main.dns_name
}

# resource "aws_ecs_cluster" "test" {
#   name = "test-cluster"
#   capacity_providers = [ "FARGATE" ]
# }

# resource "aws_ecs_service" "nginx" {
#   name            = "nginx"
#   cluster         = aws_ecs_cluster.test.id
#   task_definition = aws_ecs_task_definition.service.arn
#   desired_count   = 1
# #   iam_role        = aws_iam_role.foo.arn
# #   depends_on      = [aws_iam_role_policy.foo]

# #   ordered_placement_strategy {
# #     type  = "binpack"
# #     field = "cpu"
# #   }

#   load_balancer {
#     target_group_arn = aws_lb_target_group.test.arn
#     container_name   = "first"
#     container_port   = 80
#   }

# #   placement_constraints {
# #     type       = "memberOf"
# #     expression = "attribute:ecs.availability-zone in [us-west-2a, us-west-2b]"
# #   }
# }

# resource "aws_ecs_task_definition" "service" {
#   family = "service"
#   container_definitions = jsonencode([
#     {
#       name      = "first"
#       image     = "nginx"
#       cpu       = 5
#       memory    = 512
#       essential = true
      
#       portMappings = [
#         {
#           containerPort = 80
#           hostPort      = 80
#         }
#       ]
#     }
#   ])
# }

# resource "aws_lb_target_group" "test" {
#     depends_on = [
#       aws_lb.test
#     ]
#   name     = "nginx"
#   port     = 80
#   protocol = "HTTP"
#   vpc_id   = aws_vpc.main.id
# }

# resource "aws_lb_listener" "test" {
#   load_balancer_arn = aws_lb.test.arn
#   port              = 80
#   protocol          = "HTTP"

#   default_action {
#     type             = "forward"
#     target_group_arn = aws_lb_target_group.test.arn
#   }
# }

# resource "aws_lb_listener_rule" "test" {
#   listener_arn = aws_lb_listener.test.arn
#   priority     = 100

#   action {
#     type             = "forward"
#     target_group_arn = aws_lb_target_group.test.arn
#   }

#   condition {
#     path_pattern {
#       values = ["/*"]
#     }
#   }
# }

# resource "aws_lb" "test" {
#     depends_on = [
#       aws_internet_gateway.gw
#     ]
#   name               = "nginx"
#   internal           = false
#   load_balancer_type = "application"
# #   security_groups    = [aws_security_group.lb_sg.id]
#   subnets            = [aws_subnet.main.id, aws_subnet.main2.id]

# #   enable_deletion_protection = true

# #   access_logs {
# #     bucket  = aws_s3_bucket.lb_logs.bucket
# #     prefix  = "test-lb"
# #     enabled = true
# #   }

# #   tags = {
# #     Environment = "production"
# #   }
# }